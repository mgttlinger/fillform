{ pkgs, fillform }:

pkgs.dockerTools.buildImage {
  name = "fillformDocker";
  tag = "latest";

  fromImageName = "alpine";
  fromImageTag = "latest";

  contents = fillform;
  runAsRoot = ''
    #!${pkgs.runtimeShell}
    mkdir -p /data
  '';

  config = {
    Entrypoint = [ "/bin/fillform" ];
    WorkingDir = "/data";
    Volumes = {
      "/data" = {};
    };
  };
}
