{ haskellPackages, lib }:

let
  fillform = haskellPackages.callCabal2nix "fillform" (lib.cleanSource ./.) {};
in
fillform.overrideAttrs (old:
{ postBuild = ''
    mkdir -p $out/share/zsh/vendor-completions
    mkdir -p $out/share/bash-completion/completions
    dist/build/fillform/fillform --zsh-completion-script "$out/bin/fillform" > $out/share/zsh/vendor-completions/_fillform
    dist/build/fillform/fillform --bash-completion-script "$out/bin/fillform" > $out/share/bash-completion/completions/fillform
  '';})
