{ nixpkgs ? import <nixos-unstable> {}, compiler ? "ghc865" }:

let normalHaskellPackages = nixpkgs.pkgsMusl.haskell.packages.${compiler};

    haskellPackages = with nixpkgs.pkgs.haskell.lib; normalHaskellPackages.override {
      overrides = self: super: {
        # Dependencies we need to patch
        # hpc-coveralls = appendPatch super.hpc-coveralls (builtins.fetchurl https://github.com/guillaume-nargeot/hpc-coveralls/pull/73/commits/344217f513b7adfb9037f73026f5d928be98d07f.patch);
         hslua = dontCheck normalHaskellPackages.hslua;
       };
    };
in
rec {
  # The gen.nix file is necessary because callCabal2Nix is currently fucked in the musl tree and needs to be regenerated via cabal2nix on each change of the cabal file
  fillformStatic = (haskellPackages.callPackage ./gen.nix {}).overrideAttrs (old: { configureFlags = [
    "--ghc-option=-optl=-static"
    "--extra-lib-dirs=${nixpkgs.pkgsMusl.gmp6.override { withStatic = true; }}/lib"
    "--extra-lib-dirs=${nixpkgs.pkgsMusl.zlib.static}/lib"
    "--extra-lib-dirs=${nixpkgs.pkgsMusl.libffi.overrideAttrs (old: { dontDisableStatic = true; })}/lib"
  ];});
  fillform = nixpkgs.haskell.packages.${compiler}.callPackage ./fillform.nix {};
  fillformDocker = nixpkgs.pkgs.callPackage ./docker.nix { inherit fillform; };
}
