{ mkDerivation, aeson, aeson-better-errors, attoparsec, base
, bytestring, cassava, containers, mtl, optparse-applicative
, pandoc, pandoc-types, replace-attoparsec, stdenv
, string-conversions, text, text-metrics, vector, zlib
}:
mkDerivation {
  pname = "fillform";
  version = "0.4.4.4";
  src = ./.;
  isLibrary = false;
  isExecutable = true;
  executableHaskellDepends = [
    aeson aeson-better-errors attoparsec base bytestring cassava
    containers mtl optparse-applicative pandoc pandoc-types
    replace-attoparsec string-conversions text text-metrics vector zlib
  ];
  license = stdenv.lib.licenses.gpl3;
}
