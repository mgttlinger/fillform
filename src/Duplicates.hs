{-# LANGUAGE OverloadedStrings, BangPatterns, ScopedTypeVariables #-}

module Duplicates ( similarityAnalysis
                  , similarityMatrix
                  , filters
                  ) where

import Translation

import Control.Applicative
import Control.Monad
import Control.Monad.ST
import Control.Parallel.Strategies
import Data.STRef
import Data.Foldable
import qualified Data.Attoparsec.Text as P
import Data.Char (isDigit, isAlpha, isSpace)
import Data.List (groupBy, partition)
import Data.Maybe
import Data.Ratio
import Data.Text hiding (groupBy, length, concat, partition, singleton, maximum, foldl, zip)
import Data.Text.Metrics
import qualified Data.Vector.Unboxed.Mutable as VUM
import Replace.Attoparsec.Text
import Prelude hiding (words)

genLevenshteinNorm :: Eq a => [a] -> [a] -> Ratio Int
genLevenshteinNorm a b =
  let (dist, len) = genLevenshtein_ a b
  in if len == 0
     then 1 % 1
     else (1 % 1) - dist % len

genLevenshtein_ :: Eq a => [a] -> [a] -> (Int, Int)
genLevenshtein_ [] b = (length b, length b)
genLevenshtein_ a [] = (length a, length a)
genLevenshtein_ a b = runST $ do
  let v_len = lenb + 1
  v <- VUM.unsafeNew (v_len * 2)
  let init_with_index !i =
        when (i < v_len * 2) $ do
          VUM.unsafeWrite v i i
          init_with_index (i + 1)
      goi !i !v0 !v1 = when (i < lena) $ do
        let !ai = a !! i
            goj !j = when (j < lenb) $ do
                let !bj = b !! j
                    cost = if ai == bj then 0 else 1
                x <- (+ 1) <$> VUM.unsafeRead v (v1 + j)
                y <- (+ 1) <$> VUM.unsafeRead v (v0 + j + 1)
                z <- (+ cost) <$> VUM.unsafeRead v (v0 + j)
                VUM.unsafeWrite v (v1 + j + 1) (min x (min y z))
                goj (j + 1)
        VUM.unsafeWrite v v1 (i + 1)
        goj 0
        goi (i + 1) v1 v0
  init_with_index 0
  goi 0 0 v_len
  ld <- VUM.unsafeRead v (lenb + if even lena then 0 else v_len)
  return (ld, lenm)
  where
    lena = length a
    lenb = length b
    lenm = max lena lenb

modifyNth :: Int -> (a -> a) -> [a] -> [a]
modifyNth 0 f (a : as) = f a : as
modifyNth i f (a : as) = a : modifyNth (i - 1) f as

levenClasses :: forall a b . Eq a => (b -> [a]) -> Ratio Int -> [b] -> [[b]]
levenClasses selector cutoff = foldl merger []
  where
    fit v c = maximum $ genLevenshteinNorm (selector v) . selector <$> c
    classfit v cs = (\c -> (fit v c, c)) <$> cs
    merger :: [[b]] -> b -> [[b]]
    merger classes newvalue = runST $ do
        bestpos <- newSTRef (-1)
        bestsim <- newSTRef 0
        traverse_ (\((f,c), i) -> do
                   bsim <- readSTRef bestsim
                   when (f > cutoff && f > bsim) $ writeSTRef bestsim f *> writeSTRef bestpos i
                   ) $ zip (classfit newvalue classes) [0..]
        bpos <- readSTRef bestpos
        if bpos == -1 then return $ [newvalue] : classes
                      else return $ modifyNth bpos (newvalue :) classes

filters = words . toLower . -- filterHT . filterMention .
              filterRT . filterURL
url :: P.Parser Text
url = "http" *> (P.takeWhile $ \c -> isAlpha c || isDigit c || c == '/' || c == ':')
ht :: P.Parser Text
ht = "#" *> (P.takeWhile $ \c -> isAlpha c || isDigit c)
mention :: P.Parser Text
mention = P.try ("@" *> (P.takeWhile $ \c -> isAlpha c || isDigit c || c == '_')) <|>
                    ((P.takeWhile $ \c -> isAlpha c || isDigit c || c == '_') <* (P.takeWhile $ \c -> isSpace c || c == ':'))
filterHT = streamEdit ht $ const ""
filterURL = streamEdit url $ const ""
filterMention = streamEdit mention $ const ""
filterRT t = maybe t (\x -> x) $ stripPrefix "RT " t
square x = x * x

similarityMeasure :: (Translation, Translation) -> Ratio Int
similarityMeasure (Translation{filtered=o1}, Translation{filtered=o2}) = genLevenshteinNorm o1 o2

similarityMatrix :: [Translation] -> [[Ratio Int]]
similarityMatrix ts = withStrategy (parTraversable rpar) [[similarityMeasure (x, y) | x <- ts] | y <- ts]

similarityGroups :: [Translation] -> [[Translation]]
similarityGroups = withStrategy (parTraversable rpar) . levenClasses (filtered) 0.5 --groupBy $ curry ((> 0.45) . similarityMeasure)

concatSingletons :: [[a]] -> [[a]]
concatSingletons as = let (singletons, multi) = partition ((1 ==) . length) as in concat singletons : multi

similarityAnalysis ts = concatSingletons $ similarityGroups ts
