module Text.Pandoc.CoolWriter where

import Data.Text
import Text.Pandoc

data Cool = Bot | Conj Cool Cool

renderCool :: Cool -> Text
renderCool = undefined

panCool :: Block -> Either Text Cool
panCool = undefined

writeCool :: PandocMonad m => WriterOptions -> Pandoc -> m Text
writeCool opts (Pandoc meta blocks) = undefined
