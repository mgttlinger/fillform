{-# LANGUAGE DeriveGeneric, FlexibleContexts #-}

module Patterns where

import Prelude hiding (id)

import Control.Monad.Except
import GHC.Generics
import Data.Csv hiding (Parser, decode, encode, lookup, (.=), (.:))
import qualified Data.ByteString.Lazy as B
import Data.Text
import qualified Data.Vector as V

data PatternLine = PatternLine { id :: !Int, template :: !Text, explanation :: !Text }
  deriving (Generic, Show, Eq)

instance FromNamedRecord PatternLine
instance ToNamedRecord PatternLine
instance DefaultOrdered PatternLine

type Patterns = V.Vector PatternLine

lookupPattern :: Patterns -> Int -> Maybe Text
lookupPattern ps i = template <$> V.find ((i ==) . id) ps

preambleText :: MonadError String m =>  Patterns -> m Text
preambleText ps = case lookupPattern ps preid of
                    Just x -> return x
                    Nothing -> throwError $ "Preamble not found in patterns. Searched at id " <> show preid <> "\nMost likely this is an indicator that your version of this executable and the patterns.csv are out of sync."
  where
    preid = -9999

loadPatterns :: FilePath -> IO Patterns
loadPatterns fp = do
  fc <- B.readFile fp
  dr <- either fail return $ decodeByName fc
  return $ snd dr
