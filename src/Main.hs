{-# LANGUAGE OverloadedStrings, DeriveGeneric, RecordWildCards #-}

module Main where

import Prelude hiding (id, fail, takeWhile, lookup, putStrLn)

import Patterns
import CQP
import Cmd
import PandocHelper
import Translation
import Duplicates

import Control.Applicative
import Control.Monad hiding (fail)
import Control.Monad.Fail
import Control.Monad.Error.Class
import Control.Parallel.Strategies

import qualified Data.Attoparsec.Text as P
import Replace.Attoparsec.Text

import qualified Data.ByteString.Lazy as B
import Data.Char (isDigit, isAlpha, isSpace)
import qualified Data.Csv as C
import Data.Functor
import Data.List (groupBy, partition, zipWith)
import Data.Maybe
import Data.Monoid
import Data.String.Conversions
import Data.Text hiding (groupBy, length, concat, partition, singleton, zipWith)
import Data.Text.IO
import Data.Foldable

import Numeric

import Options.Applicative (execParser)

import Data.Aeson

preamble :: PandocMonad m => Patterns -> m Inlines
preamble ps = case preambleText ps of
                Right pr -> return $ parseAsMath pr
                Left err -> throwError $ PandocSomeError $ cs err

translate :: Text -> CQPResult -> Translation
translate pt (Result {..}) = Translation (cs $ show position) full (parseAsMath $ bindings holes pt) $ filters full

translationsTable :: [Translation] -> Blocks
translationsTable = resultsTable header . (fmap translationRow)
  where
    header = ["Position", "Tweet", "Formula"]
    translationRow (Translation {..}) = [sstr opos, sstr orig, formula]

similarityTable :: [Translation] -> Blocks
similarityTable = resultsTable [] . labelledMatrix
  where
    labelledMatrix :: [Translation] -> [[Inlines]]
    labelledMatrix ts = let labels = label <$> ts in (" \\ " : labels) : (zipWith (:) labels $ fmap cell <$> similarityMatrix ts)
    label Translation{..} = sstr orig
    cell = sstr . pack . ($ "") . showFFloat (Just 2) . fromRat . toRational

csvSimilarityMatrix :: [Translation] -> [[Text]]
csvSimilarityMatrix ts = labelledMatrix ts
  where
    labelledMatrix ts = let labels = label <$> ts in (" \\ " : labels) : (zipWith (:) labels $ fmap cell <$> similarityMatrix ts)
    label Translation{..} = opos
    cell = pack . ($ "") . showFFloat (Just 2) . fromRat . toRational


tableResults :: Patterns -> [Translation] -> IO ()
tableResults ps ts = do
  doc <- runIOorExplode $ do
    prea <- preamble ps
    writeHtml $ plain prea <> translationsTable ts
  putStrLn doc

checkResults :: Patterns -> [Translation] -> IO ()
checkResults ps ts = do
  doc <- runIOorExplode $ do
    prea <- preamble ps
    writeHtmlPage $ plain prea <> mconcat (translationsTable <$> similarityAnalysis ts) -- <> similarityTable ts -- Don't output the table as it makes the document huge and pandoc dislikes this greatly...
  putStrLn doc


convertTranslation :: PandocMonad m => Translation -> m Text
convertTranslation (Translation {..}) =
  do
    html <- writeHtmlFormula formula
    latex <- writeLaTeXFormula formula
    return $ cs $ encode $ object [ "pos" .= opos
                                  , "tweet" .= orig
                                  , "html" .= html
                                  , "latex" .= latex
                                  ]

scriptResults :: [Translation] -> IO ()
scriptResults = traverse_ (putStrLn <=< runIOorExplode . convertTranslation)

printPreamble :: Patterns -> IO ()
printPreamble = putStrLn <=< runIOorExplode . (writeHtml . plain <=< preamble)


fillPatterns :: Locations -> (Patterns -> [Translation] -> IO ()) -> IO ()
fillPatterns (Locations {..}) cnt = do
  pats <- loadPatterns patternFile
  CQPResultsFile { pattern = p, result = CQPResults {..} } <- loadResults resultsFile
  case lookupPattern pats p of
    Just pt -> cnt pats $ (translate pt) <$> matches
    Nothing -> fail $ "Pattern " <> show p <> " not found"

program :: Command -> IO ()
program (Preamble p) = printPreamble =<< loadPatterns p
program (Check l) = fillPatterns l checkResults
program (Script l) = fillPatterns l $ const scriptResults
program (HTable l) = fillPatterns l tableResults
program (Matrix l) = fillPatterns l $ const $ B.putStrLn . C.encode . csvSimilarityMatrix
program (ChkJSON j) = loadResults j $> ()

main :: IO ()
main = program =<< execParser args
