module Cmd ( Command(..)
           , Locations(..)
           , args
           ) where

import Options.Applicative

_patternFile :: Parser FilePath
_patternFile = strArgument $
  metavar "PATTERNFILE"
  <> help "The csv file defining all the formula patterns"
  <> action "file"

_resultsFile :: Parser FilePath
_resultsFile = strArgument $
  metavar "RESULTSFILE"
  <> help "The json.gz file with the CQP results"
  <> action "file"

data Locations = Locations { patternFile :: FilePath, resultsFile :: FilePath }

_locations :: Parser Locations
_locations = Locations <$> _patternFile <*> _resultsFile



data Command = Check Locations
             | Preamble FilePath
             | Script Locations
             | HTable Locations
             | Matrix Locations
             | ChkJSON FilePath

args :: ParserInfo Command
args = info (hsubparser (mconcat commands) <**> helper) $ header "fillform - A programm to fill formula patterns from CQP results"
  where
    commands = [ chelper "check"    (Check    <$> _locations) "generate a HTML5 page visualiting the results for human inspection"
               , chelper "preamble" (Preamble <$> _patternFile) "print the HTML5 code for the preamble necessary to render the formulas"
               , chelper "script"   (Script   <$> _locations) "generate JSON output of the results for further processing"
               , chelper "table"    (HTable   <$> _locations) "generate a HTML5 table and preamble visualiting the results for human inspection"
               , chelper "matrix"   (Matrix   <$> _locations) "generate the csv similarity matrix"
               , chelper "chk_json" (ChkJSON  <$> _resultsFile) "check if the JSON file parses"
               ]
    chelper n cs d = command n $ info cs $ progDesc d
