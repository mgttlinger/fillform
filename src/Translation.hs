{-# LANGUAGE DeriveGeneric #-}

module Translation where

import Data.Text
import GHC.Generics
import Text.Pandoc.Builder

data Translation = Translation { opos :: !Text, orig :: !Text, formula :: !Inlines, filtered :: ![Text]}
  deriving (Generic, Show, Eq)
