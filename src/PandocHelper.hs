{-# LANGUAGE OverloadedStrings, FlexibleContexts #-}
module PandocHelper ( Blocks
                    , Inlines
                    , Pandoc
                    , PandocMonad
                    , PandocError(PandocSomeError)
                    , fromList
                    , parseAsMath
                    , plain
                    , resultsTable
                    , runIOorExplode
                    , singleton
                    , sstr
                    , writeHtml
                    , writeHtmlFormula
                    , writeHtmlPage
                    , writeLaTeXFormula
                    ) where

import Control.Monad

import Data.Either
import Data.Functor.Identity
import Data.String.Conversions
import Data.Text (Text)

import Text.Pandoc
import Text.Pandoc.Builder
import Text.Pandoc.Error
import Text.Pandoc.Writers.Math


docu :: Blocks -> Pandoc
docu = setTitle "Results" . doc

resultsTable :: [Inlines] -> [[Inlines]] -> Blocks
resultsTable hs rs = simpleTable (plain <$> hs) ((plain <$>) <$> rs)

html5Cfg = (def {writerHTMLMathMethod = MathJax "https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js"})

writeHtml :: PandocMonad m => Blocks -> m Text
writeHtml = writeHtml5String html5Cfg . doc

writeHtmlFormula :: PandocMonad m => Inlines -> m Text
writeHtmlFormula = writeHtml . plain

ctp :: Text -> Template Text
ctp = fromRight undefined . runIdentity . compileTemplate ""

-- inlined and slightly shortened from pandoc template for portability
tmpl :: Template Text
tmpl = ctp "<!DOCTYPE html><html xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"$lang$\" xml:lang=\"$lang$\"$if(dir)$ dir=\"$dir$\"$endif$><head><meta charset=\"utf-8\" /><meta name=\"generator\" content=\"pandoc\" /><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0, user-scalable=yes\" />$for(author-meta)$<meta name=\"author\" content=\"$author-meta$\" />$endfor$ $if(date-meta)$<meta name=\"dcterms.date\" content=\"$date-meta$\" />$endif$ <title>$if(title-prefix)$$title-prefix$ – $endif$$pagetitle$</title><style>code{white-space: pre-wrap;} span.smallcaps{font-variant: small-caps;} span.underline{text-decoration: underline;} div.column{display: inline-block; vertical-align: top; width: 50%;}$if(quotes)$ q { quotes: \"“\" \"”\" \"‘\" \"’\"; }$endif$</style>$if(highlighting-css)$ <style>$highlighting-css$</style>$endif$ $for(css)$<link rel=\"stylesheet\" href=\"$css$\" />$endfor$ $if(math)$ $math$ $endif$ <!--[if lt IE 9]> <script src=\"//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js\"></script><![endif]-->$for(header-includes)$ $header-includes$ $endfor$</head><body>$for(include-before)$ $include-before$ $endfor$ $if(title)$<header id=\"title-block-header\"><h1 class=\"title\">$title$</h1>$if(subtitle)$<p class=\"subtitle\">$subtitle$</p>$endif$ $for(author)$<p class=\"author\">$author$</p>$endfor$ $if(date)$<p class=\"date\">$date$</p>$endif$</header>$endif$ $if(toc)$ <nav id=\"$idprefix$TOC\" role=\"doc-toc\">$table-of-contents$</nav>$endif$ $body$ $for(include-after)$ $include-after$ $endfor$</body></html>"

htmlPage = html5Cfg { writerTemplate = Just tmpl }

writeHtmlPage :: PandocMonad m => Blocks -> m Text
writeHtmlPage = writeHtml5String htmlPage . docu

writeLaTeXFormula :: PandocMonad m => Inlines -> m Text
writeLaTeXFormula = writeLaTeX def . doc . plain

parseAsMath :: Text -> Inlines
parseAsMath = math . cs

sstr :: ConvertibleStrings a Text => a -> Inlines
sstr = str . cs
