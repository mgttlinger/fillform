{-# LANGUAGE DeriveGeneric, OverloadedStrings #-}
module CQP where

import Codec.Compression.GZip

import Data.Aeson
import Data.Aeson.BetterErrors
import qualified Data.Attoparsec.Text as P
import qualified Data.ByteString.Lazy as B
import Data.Char
import Data.Maybe
import qualified Data.Map as M
import Data.List (isSuffixOf)
import Data.String.Conversions
import Data.Text hiding (isSuffixOf, foldl)


import GHC.Generics

import Replace.Attoparsec.Text

type Bindings = M.Map Text Text

data CQPResult = Result { position :: !Int, holes :: !Bindings, full :: !Text }
  deriving (Generic, Show, Eq)
data CQPResults = CQPResults { matches :: [CQPResult] }
  deriving (Generic, Show, Eq)
data CQPResultsFile = CQPResultsFile { pattern :: !Int, result :: CQPResults }
  deriving (Generic, Show, Eq)

instance FromJSON CQPResultsFile

asResultsFile :: Parse e CQPResultsFile
asResultsFile = CQPResultsFile
  <$> key "pattern" asIntegral
  <*> key "result" asResults

instance FromJSON CQPResults

asResults :: Parse e CQPResults
asResults = CQPResults <$> key "matches" (eachInArray asResult)

instance FromJSON CQPResult

asResult :: Parse e CQPResult
asResult = Result
  <$> key "position" asIntegral
  <*> key "holes" (M.fromList . foldl (\ accu el -> case el of
                                                      (k, String v) -> (k,v) : accu
                                                      _ -> accu) [] <$> eachInObject asValue)
  <*> key "full" asText


escapeSpecials :: Text -> Text
escapeSpecials = replfunction replacements
  where
    replacements = [ ("\\", "\\textbackslash{}")
                   , ("^", "\\textasciicircum{}")
                   , ("~", "\\textasciitilde{}")
                   , ("}", "\\}")
                   , ("{", "\\{")
                   , ("_", "\\_")
                   -- , ("#", "\\#") -- see https://github.com/mathjax/MathJax/issues/2367
                   , ("%", "\\%")
                   , ("&", "\\&")
                   , ("$", "\\$")
                   ]
    replfunction [] = id
    replfunction ((s, r) : rs) = replfunction rs . replace s r

bindingFn :: Bindings -> Text -> Text
bindingFn bs c = "\\text{" <> escapeSpecials (fromMaybe (enhole c) $ M.lookup c bs) <> "}"

identifier :: P.Parser Text
identifier = P.takeWhile $ \c -> isAlpha c || isDigit c

enhole :: Text -> Text
enhole t = "{?" <> t <> "}"

holeParser :: P.Parser Text
holeParser = "{?" *> identifier <* P.skipWhile ('}' /=)  <* "}"

repeatedHoleParser :: P.Parser Text
repeatedHoleParser = "?" *> identifier


bindings :: Bindings -> Text -> Text
bindings bs = let bdgs = bindingFn bs in streamEdit repeatedHoleParser bdgs . streamEdit holeParser bdgs

loadResults :: FilePath -> IO CQPResultsFile
loadResults fp = do
  s <- (if isSuffixOf "gz" fp then decompress else id) <$> B.readFile fp
  either (fail . ("JSON decode failed: \n" <>). unpack . Data.Text.unlines . displayError') pure $ parse asResultsFile $ cs s
